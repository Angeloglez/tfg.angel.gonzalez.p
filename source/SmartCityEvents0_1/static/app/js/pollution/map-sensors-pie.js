function actionSelect(selector, feature, map, pixel_id, view, mode){
	centerSensor(view, map, pixel_id, mode);
	selectSensor(selector, feature);
	load_sensorPie();
}

function actionClickFeature(selector, feature, map, pixel, view){
    actionSelect(selector, feature, map, pixel,view, "click");    
}


function actionClickNoFeature(selector, feature, map, pixel, view){
	//do nothing 
}

function load_Empty(){
	load_sensorPie_Empty();
	unselectSensor();
}