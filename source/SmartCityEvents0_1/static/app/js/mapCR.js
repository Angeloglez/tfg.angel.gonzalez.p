function createPopUp(){
	var container = document.getElementById('popup');
	var closer = document.getElementById('popup-closer');

	closer.onclick = function() {
		popup.setPosition(undefined);
		closer.blur();
		return false;
	};

	var popup = new ol.Overlay(({
	  	element: container,
		autoPan: true,
		autoPanAnimation: {
			duration: 250
		}
	}));

	return popup;
}

function bounce(t) {
	var s = 7.5625, p = 2.75, l;
	if (t < (1 / p)) {
	    l = s * t * t;
	} else {
	    if (t < (2 / p)) {
	    	t -= (1.5 / p);
	      	l = s * t * t + 0.75;
	    } else {
	      	if (t < (2.5 / p)) {
	        	t -= (2.25 / p);
	        	l = s * t * t + 0.9375;
	      	} else {
	        	t -= (2.625 / p);
	        	l = s * t * t + 0.984375;
	      	}
	    }
	}
	return l;
}

function addgraph (content, idsensor) {
    var div_graph = document.createElement("div");
    div_graph.setAttribute("class","sensorgraph");
    content.appendChild(div_graph);

    $.get("/pollution/map/datapopup/", {'idsensor': idsensor}, function(data){
        $('.sensorgraph').html(data);
    });

}

$(document).ready(function() {

	var content = document.getElementById('popup-content');
	var popup = createPopUp();

	window.app = {};
    var app = window.app;
	app.CentreCRControl = function(opt_options) {

        var options = opt_options || {};

        var button = document.createElement('button');
        button.innerHTML = 'o';

        var this_ = this;
        var handleCentreCR = function() {
          	var pan = ol.animation.pan({
        		duration: 2000,
        		easing: bounce,
        		source: (viewCR.getCenter())
    		});
    		mapCR.beforeRender(pan);
    		viewCR.setCenter(ol.proj.transform([-3.9279, 38.9849],'EPSG:4326', 'EPSG:900913'));
        };

        button.addEventListener('click', handleCentreCR, false);

        var element = document.createElement('div');
        element.className = 'centreCR ol-unselectable ol-control';
        element.appendChild(button);

        ol.control.Control.call(this, {
          element: element,
          target: options.target
        });

    };
    app.NotificationsControl = function(opt_options) {

        var options = opt_options || {};

        var button = document.createElement('button');
        button.innerHTML = '!';

        //IF NOT NOTIFICATIONS, BUTTON WILL BE HIDDEN
        if($('.notificationDisabled').val() == "True"){
            button.style.display = 'none';
        }

        var this_ = this;
        var handleNotifications = function() {
            windowPopupNotifications($('.typePollution').val());
            button.style.display = 'none';
        };

        button.addEventListener('click', handleNotifications, false);

        var element = document.createElement('div');
        element.className = 'notifications ol-unselectable ol-control';
        element.appendChild(button);

        ol.control.Control.call(this, {
          element: element,
          target: options.target
        });

    };

    ol.inherits(app.CentreCRControl, ol.control.Control);
    ol.inherits(app.NotificationsControl, ol.control.Control);

   	//To configure a view
    var viewCR = new ol.View({
	    center: ol.proj.transform([-3.9279, 38.9849],'EPSG:4326', 'EPSG:900913'),
	    zoom: 14,
	    minZoom: 14,
	    maxZoom:18
	})

    //To create base layer
    var baseLayer = new ol.layer.Tile({
    	source: new ol.source.OSM()
    	//source: new ol.source.MapQuest({ layer: 'osm'})
    });

    //To create map
    var mapCR = new ol.Map({
	    target: 'map_main',

	    interactions: ol.interaction.defaults().extend([
       		new ol.interaction.DragRotateAndZoom()
       	]),

	    controls: new ol.control.defaults({
        	attribution: false,							//Change for show attribution by OSM
        	attributionOptions: ({
	            collapsible: false
	        })      
        }).extend([
        	new app.CentreCRControl(),
        	new app.NotificationsControl(),
       		new ol.control.FullScreen(),
       		new ol.control.Rotate()
       	]),

        renderer: 'canvas',
        loadTilesWhileAnimating: true,
        layers: [baseLayer],
        overlays: [popup],
        view: viewCR
	});

    loadSensors(mapCR);
	
	mapCR.on('singleclick', function(evt) {
		var feature = mapCR.forEachFeatureAtPixel(evt.pixel,
		function(feature, layer) {
			return feature;
		});
		if(feature) {
			var coordinate = evt.coordinate;
			var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326'));
            var featureName = feature.get('name');
			var featureText = feature.get('text');
			var featureValue = feature.get('value');
            var featureAlert = feature.get('alert');
			content.innerHTML = '<code>' + featureName + '<br/>' + featureText + '   (' + hdms + ')<br>' + 'Pollution: '+ featureValue + featureAlert + '</code>';
            addgraph (content, feature.get('sensor'));
			popup.setPosition(coordinate);
		}
	});
});

