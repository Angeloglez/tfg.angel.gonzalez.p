function styleColor(value, alert){
	//To calcule color for value of sensor
	//range value of sensor: 0 to 50
	var mediumv = alert*0.7; //this value is the medium value of colors. Calc to 70% of alert
	var r = 255; var g = 255; var o=0.6;

   	if(value>mediumv){
   		g = Math.round(g - (value-mediumv)*255/mediumv);
	} else if (value>0 && value<mediumv){
		r = Math.round(r - (mediumv-value)*225/mediumv);
	} else if (value == 0){
		r = 0;
	}

   	if(value<0){ r=g=0; o=0.8}

   	return 'rgba(' + r +',' + g + ', 0 ,' + o +')';  

}

function createStyle(text, color, error){
	if (error)
		return new ol.style.Style({
	   		stroke: new ol.style.Stroke({
		    	color: color,
		    	width: 1
		    }),
		    fill: new ol.style.Fill({
		    	color: color
		    }),
		});
	else 
		return new ol.style.Style({
	   		stroke: new ol.style.Stroke({
		    	color: color,
		    	width: 1
		    }),
		    fill: new ol.style.Fill({
		    	color: color
		    }),
		    text: new ol.style.Text({
				font: 'Bold 30px Arial',
				text: text,
				fill: new ol.style.Fill({color: 'rgba(255, 255, 255, 0.6)'}),
				stroke: new ol.style.Stroke({color: 'rgba(0, 0, 255, 0.6)', width: 2})
			})
		});

}

function loadSensors(mapCR){

	var mediumv= 50; //this value is the medium value of sensors
	//range value of sensor: 0 to 100
	//To carry values of inputs. Values inputs equal to values of database
	var sensors = [];
	var data = document.getElementById('sensorData').getElementsByTagName('input');
	for(i=0; i<data.length; i=i+7){ 
		sensors.push([parseInt(data[i].value), parseFloat(data[i+1].value.replace(",",".")), parseFloat(data[i+2].value.replace(",",".")), parseFloat(data[i+3].value.replace(",",".")), data[i+4].value, data[i+5].value, parseFloat(data[i+6].value.replace(",","."))]);
	}

	//To create circle zone of sensors  	
    var error = false; var radius = 800;
    for(i=0; i<sensors.length;i++){
    	var listFeature = [];
    	if(sensors[i][3]<0){
    		error = true;
    		radius = 200;
    	}else{
    		error = false;
    		radius = 800;
    	}

	   	var circlefeature = new ol.Feature({
			geometry: new ol.geom.Circle(ol.proj.transform([sensors[i][1], sensors[i][2]],'EPSG:4326', 'EPSG:900913'), radius, 0),
			text: 'Sensor id: ' + sensors[i][0].toString(),
			value: sensors[i][3],
			sensor: sensors[i][0],
			name: sensors[i][4].toString(),
			desc: sensors[i][5].toString(),
			alert: ' (Alert value: ' + sensors[i][6].toString() + ')'
		})            
	   	listFeature.push(circlefeature);

	   	if(error){
	   		circlefeature.set('value', "ERROR, DATA NOT RECIVED!");
	   		circlefeature.set('alert', "");
		  	var iconPointFeature = new ol.Feature({
			  	geometry: new ol.geom.Circle(ol.proj.transform([sensors[i][1], sensors[i][2]-0.0008],'EPSG:4326', 'EPSG:900913'), 30, 0)
			});

			var iconLineFeature = new ol.Feature({
				geometry: new ol.geom.LineString([
					ol.proj.transform([sensors[i][1], sensors[i][2]+0.0007], 'EPSG:4326', 'EPSG:900913'),
					ol.proj.transform([sensors[i][1], sensors[i][2]-0.0002], 'EPSG:4326', 'EPSG:900913')
				]),
			});
			var iconLine2Feature = new ol.Feature({
				geometry: new ol.geom.LineString([
					ol.proj.transform([sensors[i][1], sensors[i][2]+0.0007], 'EPSG:4326', 'EPSG:900913'),
					ol.proj.transform([sensors[i][1], sensors[i][2]-0.0002], 'EPSG:4326', 'EPSG:900913')
				]),
			});

			var iconPointStyle = new ol.style.Style({
	  			stroke: new ol.style.Stroke({
			    	color: 'rgba(255, 255, 255, 1)'
		    	}),
		    	fill: new ol.style.Fill({
		    		color: 'rgba(255, 0, 0, 1)'
		    	})
			});

			var iconLineStyle = new ol.style.Style({
	  			stroke: new ol.style.Stroke({
			    	color: 'rgba(255, 255, 255, 1)',
			    	width: 6
		    	}),
			});
			var iconLine2Style = new ol.style.Style({
	  			stroke: new ol.style.Stroke({
			    	color: 'rgba(255, 0, 0, 1)',
			    	width: 5
		    	}),
			});


			iconPointFeature.setStyle(iconPointStyle);
			iconLineFeature.setStyle(iconLineStyle);
			iconLine2Feature.setStyle(iconLine2Style);

			listFeature.push(iconPointFeature);
			listFeature.push(iconLineFeature);
			listFeature.push(iconLine2Feature);
		}

		var sensorZone = new ol.layer.Vector({
			source: new ol.source.Vector({
				features: listFeature
			}),
			style: createStyle(sensors[i][3].toString(), styleColor(sensors[i][3], sensors[i][6]), error)

		});
		mapCR.addLayer(sensorZone);

	}
}