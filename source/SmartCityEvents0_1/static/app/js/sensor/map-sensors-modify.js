function actionSelect(selector, feature, map, pixel_id, view, mode){
	centerSensor(view, map, pixel_id, mode);
    selectSensor(selector, feature);
    put_values_remove(feature.get('sensor'), feature.get('type'), feature.get('longitude'), feature.get('latitude'), feature.get('name'), feature.get('desc'), feature.get('alert'));
}

function actionClickFeatureModify(selector, feature, map, pixel, view){
    actionSelect(selector, feature, map, pixel,view, "click");    
}

function actionClickNoFeatureModify(selector, feature, map, pixel, view){
    //do nothing 
}

function load_Empty(){
	put_values_remove("", null, "", "", "", "", "");
	unselectSensor();
}