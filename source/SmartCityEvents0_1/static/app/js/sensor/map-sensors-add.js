function actionSelect(selector, feature, map, pixel, view){
	//do nothing
}

function actionClickFeature(selector, feature, map, pixel, view){
	var position = map.getCoordinateFromPixel(pixel);
    position = ol.proj.transform(position, 'EPSG:900913', 'EPSG:4326');
    addSelected(position, map);
    put_values_add(position[0].toFixed(5), position[1].toFixed(5));
    enable_inputs();  
}

function actionClickNoFeature(selector, feature, map, pixel, view){
	actionClickFeature(selector, feature, map, pixel, view);
}

function load_Empty(){
    //do nothing
}


function initialSelected(feature){
	//do nothing
}

var addSelectedradius = 50;
var addSelectedfeature = null;
var addSelectedListFeature = [];
var addSelectedVector = null;
var currentVector = null;

function addSelectedStyle(){
	return new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'rgba(0,0,255,0.9)',
            width: 2
        }),
        fill: new ol.style.Fill({
            color: 'rgba(0,0,255,0.2)'
        })
    });
}

function addSelected(position, map){

	if(currentVector != null){
		map.removeLayer(currentVector);
	}
	

	addSelectedListFeature = [];

	//To create circle zone of sensors  	
    addSelectedfeature = new ol.Feature({
        geometry: new ol.geom.Circle(ol.proj.transform(position,'EPSG:4326', 'EPSG:900913'), addSelectedradius, 0),
        longitude: position[0],
        latitude: position[1]
    });
    
    addSelectedfeature.setStyle(addSelectedStyle());           
   
   	addSelectedListFeature.push(addSelectedfeature);
	var addSelectedVector = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: addSelectedListFeature
        }),
    });

    map.addLayer(addSelectedVector);

    currentVector = addSelectedVector;

}