function create_mapsensors_add() {
	$.get("/management/sensors/add/map/", function(data){
        $('#mapsensors').html(data);
    });
}

function create_mapsensors_remove() {
	$.get("/management/sensors/remove/map/", function(data){
        $('#mapsensors').html(data);
    });
}

function create_mapsensors_modify() {
	$.get("/management/sensors/modify/map/", function(data){
        $('#mapsensors').html(data);
    });
}

function put_values_add (lon, lat){
    $('#newlon').val(lon);
    $('#newlat').val(lat);
}

function put_values_remove(idsensor, type, lon, lat, name, desc, alert){
	$('#selectSensor').val(idsensor);
	$('#id').val(idsensor);
	if(type == null){
		$('#type').val(null);
	}
	else if(type == 0){
		$('#type').val("Pollution - NO2");
	}
	else if(type == 1){
		$('#type').val("Pollution - SO2");
	}
	else if(type == 2){
		$('#type').val("Pollution - CO2");
	}
	else if(type == 3){
		$('#type').val("Pollution - O3");
	}
	else if(type == 4){
		$('#type').val("Pollution - PM2.5");
	}
	else if(type == 5){
		$('#type').val("Pollution - PM10");
	}
	$('#name').val(name);
	$('#alert').val(alert);
    $('#lon').val(lon);
    $('#lat').val(lat);
    $('#desc').val(desc);
}		

function enable_inputs(){
	$('#newlatcheck').prop('checked', true);
	$('#newloncheck').prop('checked', true);
	$('#newlon').prop('disabled', false);
    $('#newlat').prop('disabled', false);
}