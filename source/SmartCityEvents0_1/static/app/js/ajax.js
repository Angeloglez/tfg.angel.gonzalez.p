var res;

function inicializa_xhr() {

  if(window.XMLHttpRequest) {
    res = new XMLHttpRequest();
  }
  else if(window.ActiveXObject) {
    res = new ActiveXObject("Microsoft.XMLHTTP");
  }
}

function startRequest(url, metodo, func){
  res.onreadystatechange = func;
  res.open(metodo, url , true);
  res.send();
}

function load(url){
  inicializa_xhr();
  startRequest(url, 'GET', action);
}

function action(){       
  if (res.readyState == 4 && res.status == 200) {
    eval(res.responseText);
  }
}

function startAjax(url){
  load(url);
  //interval = setInterval(function(){load(url);}, 5000);
}