var sensorSelected = null;
var radius_standar = 250;
var radius_selected = 300;
var radius_coverage = 800;

function bounce(t) {
    var s = 7.5625, p = 2.75, l;
    if (t < (1 / p)) {
        l = s * t * t;
    } else {
        if (t < (2 / p)) {
            t -= (1.5 / p);
            l = s * t * t + 0.75;
        } else {
            if (t < (2.5 / p)) {
                t -= (2.25 / p);
                l = s * t * t + 0.9375;
            } else {
                t -= (2.625 / p);
                l = s * t * t + 0.984375;
            }
        }
    }
    return l;
}

function coordinatesSensor(id_sensor){
    coordinates = null;
    var data = document.getElementById('sensorData').getElementsByTagName('input');
    for(i=0; i<data.length; i=i+7){ 
        if(data[i].value == id_sensor.toString()){
            coordinates = [parseFloat(data[i+1].value.replace(",",".")), parseFloat(data[i+2].value.replace(",","."))]
        }
    }
    return coordinates;
}

function featureSensor(features, id_sensor){
    var feature = null;
    for(i=0; i<features.length;i++){
        if(features[i].get('sensor') == id_sensor){
            feature = features[i];
        }
    }

    return feature;
}


function createStyleSelected(text){
    return new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'rgba(0,0,255,0.9)',
            width: 2
        }),
        fill: new ol.style.Fill({
            color: 'rgba(0,0,255,0.2)'
        }),
        text: new ol.style.Text({
            font: 'Bold 15px Arial',
            text: text,
            fill: new ol.style.Fill({color: 'rgba(255, 255, 255, 0.6)'}),
            stroke: new ol.style.Stroke({color: 'rgba(0, 0, 255, 0.6)', width: 2})
        })
    });

}

function createStyleUnselected(text, type = 0){
    color_type = 'rgba(200,50,255,0.6)'// #C832FF
    if(type == 1) color_type = 'rgba(206,209,25,0.6)' //  #ced119 
    else if(type == 2) color_type = 'rgba(94,123,27,0.6)' //  #5e7b1b
    else if(type == 3) color_type = 'rgba(17,122,101,0.6)' // #117a65
    else if(type == 4) color_type = 'rgba(209,83,25,0.6)' //  #d15319   
    else if(type == 5) color_type = 'rgba(94,48,26,0.6)' // #5e301a

    return new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: color_type,
            width: 2
        }),
        fill: new ol.style.Fill({
            color: color_type
        }),
        text: new ol.style.Text({
            font: 'Bold 15px Arial',
            text: text,
            fill: new ol.style.Fill({color: 'rgba(255, 255, 255, 0.6)'}),
            stroke: new ol.style.Stroke({color: 'rgba(0, 0, 255, 0.6)', width: 2})
        })
    });
}

function createStyleCoverage(){
    return new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'rgba(0,0,0,0.2)',
            width: 2,
            lineDash: [10,5]
        }),
    });
}

function centerSensor(view, map, pixel_id, mode){
    var pan = ol.animation.pan({
        duration: 2000,
        easing: bounce,
        source: (view.getCenter())
    });
    map.beforeRender(pan);
    if(mode == "click"){
        var feature = map.forEachFeatureAtPixel(pixel_id, function(feature, layer) { return feature; });
        pixel_id = feature.get('sensor');
    }
    view.setCenter(ol.proj.transform(coordinatesSensor(pixel_id),'EPSG:4326', 'EPSG:900913'));    
}

function selectSensor(selector, feature){
    if(sensorSelected != null){
        sensorSelected.setStyle(createStyleUnselected(sensorSelected.get('sensor').toString()));
        sensorSelected.getGeometry().setRadius(radius_standar
            );
    }

    selector.val(parseInt(feature.get('sensor')));
    feature.setStyle(createStyleSelected(feature.get('sensor').toString()));
    feature.getGeometry().setRadius(radius_selected);
    sensorSelected = feature;
}

function unselectSensor(){
    if(sensorSelected != null){
        sensorSelected.setStyle(createStyleUnselected(sensorSelected.get('sensor').toString()));
        sensorSelected.getGeometry().setRadius(radius_standar);
        sensorSelected = null;
    }
}

function loadSensors(map){
	var sensors = [];
	var data = document.getElementById('sensorData').getElementsByTagName('input');
	for(i=0; i<data.length; i=i+7){ 
		sensors.push([parseInt(data[i].value), parseFloat(data[i+1].value.replace(",",".")), parseFloat(data[i+2].value.replace(",",".")), parseInt(data[i+3].value), data[i+4].value, data[i+5].value, parseFloat(data[i+6].value)]);
	}

	//To create circle zone of sensors  	
    var circlefeature;
    var circleCoverage;
    var listFeature = [];
    var listFeatureCoverage = [];
    
    if(sensors.length>0){
        circlefeature = new ol.Feature({
            geometry: new ol.geom.Circle(ol.proj.transform([sensors[0][1], sensors[0][2]],'EPSG:4326', 'EPSG:900913'), radius_standar, 0),
            sensor: sensors[0][0],
            longitude: sensors[0][1],
            latitude: sensors[0][2],
            type: sensors[0][3],
            name: sensors[0][4],
            desc: sensors[0][5],
            alert: sensors[0][6]
        }) 
        circlefeature.setStyle(createStyleUnselected(sensors[0][0].toString(), sensors[0][3]));           
        listFeature.push(circlefeature);

        circleCoverage = new ol.Feature({
            geometry: new ol.geom.Circle(ol.proj.transform([sensors[0][1], sensors[0][2]],'EPSG:4326', 'EPSG:900913'), radius_coverage, 0),
        }) 
        circleCoverage.setStyle(createStyleCoverage());
        listFeatureCoverage.push(circleCoverage);           

    }
    

    for(i=1; i<sensors.length;i++){
    	circlefeature = new ol.Feature({
			geometry: new ol.geom.Circle(ol.proj.transform([sensors[i][1], sensors[i][2]],'EPSG:4326', 'EPSG:900913'), radius_standar, 0),
			sensor: sensors[i][0],
			longitude: sensors[i][1],
			latitude: sensors[i][2],
			type: sensors[i][3],
            name: sensors[i][4],
            desc: sensors[i][5],
            alert: sensors[i][6]
		}) 
        circlefeature.setStyle(createStyleUnselected(sensors[i][0].toString(), sensors[i][3]));           
	   	listFeature.push(circlefeature);

        circleCoverage = new ol.Feature({
            geometry: new ol.geom.Circle(ol.proj.transform([sensors[i][1], sensors[i][2]],'EPSG:4326', 'EPSG:900913'), radius_coverage, 0),
        }) 
        circleCoverage.setStyle(createStyleCoverage());
        listFeatureCoverage.push(circleCoverage);   
    }

    var sensorsLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: listFeature
        }),
    });

    var coverageLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: listFeatureCoverage
        }),
    });

    map.addLayer(coverageLayer);
    map.addLayer(sensorsLayer);

    return listFeature;
}